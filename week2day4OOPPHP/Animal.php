<?php

class Animal
{
    public $name = "";
    public $legs = 4;
    public $cold_blooded = "no";

    public function __construct($name)
    {
        return $this->name = $name;
    }

    public function getName()
    {
        return $this->name . "<br>";
    }

    public function getLegs()
    {
        return $this->legs . "<br>";
    }

    public function getCold_Blooded()
    {
        return $this->cold_blooded . "<br>";
    }
}